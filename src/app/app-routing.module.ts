import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionsFormComponent } from './questions-form/questions-form.component';

const routes: Routes = [
  { path: '', component: QuestionsFormComponent, pathMatch: 'full' },
  { path: 'preview', loadChildren: () => import('./preview/preview.module').then(m => m.PreviewModule) },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
