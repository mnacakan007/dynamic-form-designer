import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {take, takeLast} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionFormService {
  questionForms: any[] = [];
  questionFormData: any[] = [];
  questionFormIds: number[] = [];
  addDynamicFormComp = false;
  oldId: number;

  submitDynamicForm$: BehaviorSubject<boolean> = new BehaviorSubject(null);
  formState$: ReplaySubject<any> = new ReplaySubject(null);

  constructor() {
  }

  save() {
    this.submitDynamicForm$.next(true);
    this.formState$.
    subscribe(state => {
      if (!state) {
        return;
      }

      console.log(state);
    });
  }

  watchFormSubmitClick(): Observable<boolean> {
    return this.submitDynamicForm$.asObservable();
  }

  addForm() {
    this.addDynamicFormComp = true;
    const d = new Date();
    const milliseconds = d.getMilliseconds();
    this.questionForms.push({id: milliseconds});
    this.oldId = this.questionForms[this.questionForms.length - 1].id;
    this.formState$.next(this.questionFormData);
  }

  removeDynamicFormById(id) {
    this.questionFormData = this.questionFormData.filter(f => f.id !== id);
    this.formState$.next(this.questionFormData);
  }
}
