import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuestionFormService} from '../services/question-form.service';


@Component({
  selector: 'app-questions-form',
  templateUrl: './questions-form.component.html',
  styleUrls: ['./questions-form.component.scss']
})
export class QuestionsFormComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public questionFormService: QuestionFormService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [new Date().getMilliseconds()],
      formName: ['New form', [Validators.required]],
      description: ['', [Validators.required]],
    });

    this.watchSubmitClick();
  }


  removeForm(id: number) {
    this.questionFormService.questionForms = this.questionFormService.questionForms.filter(f => f.id !== id);
  }

  watchSubmitClick() {
    this.questionFormService.watchFormSubmitClick()
      .subscribe((click: boolean) => {

        if (!click) {
          return;
        }

        this.questionFormService.questionFormData.push(this.form.value);

        this.questionFormService.questionFormData.forEach((v) => {
          if (v.formName) {
            this.questionFormService.questionFormData = [];
            this.questionFormService.questionFormData.push(this.form.value);
          } else  {
            this.questionFormService.questionFormData.push(this.form.value);
          }
        });

        if (!this.questionFormService.addDynamicFormComp) {
          this.questionFormService.submitDynamicForm$.next(null);
        }
      });
  }

}
