import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {of} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpEventType, HttpRequest} from '@angular/common/http';
import {catchError, last, map, tap} from 'rxjs/operators';
import {MatSelectChange} from '@angular/material/select';
import {FileUploadModel, QuestionForms} from '../../models/models';
import {QuestionFormService} from '../../services/question-form.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';


@Component({
  selector: 'app-questions-form-options',
  templateUrl: './questions-form-options.component.html',
  styleUrls: ['./questions-form-options.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class QuestionsFormOptionsComponent implements OnInit, AfterViewInit {

  @Input() questionForm: QuestionForms;
  @Input() text = 'Upload';
  @Input() param = 'file';
  @Input() target = 'https://file.io';
  @Input() accept = 'image/*';
  @Output() complete = new EventEmitter<string>();
  @Output() onRemoveQuestionForm = new EventEmitter<number>();
  @Output() submitDynamicQuestionForm = new EventEmitter<FormGroup>();

  files: Array<FileUploadModel> = [];

  dynamicForm: FormGroup;
  formTypes = ['short_answer', 'paragraph', 'multi', 'checkbox', 'dropdown', 'file', 'date'];
  questionTypeName = 'multi';
  questionOptions = ['Dropdown Option 1', 'Dropdown Option 2', 'Dropdown Option 3'];
  answerOptions: FormArray;
  initDate = new Date();
  dropdownValue = 'Dropdown Option 1';


  constructor(private formBuilder: FormBuilder,
              public questionFormService: QuestionFormService,
              private _http: HttpClient) {
  }

  ngOnInit() {
    this.dynamicForm = this.formBuilder.group({
      id: [''],
      type: ['', Validators.required],
      question: ['', Validators.required],
      answerOptions: new FormArray([]),
      dropdown: [`${this.questionOptions[0]}`],
      file: [''],
      date: [''],
    });

    this.dynamicForm.patchValue({id: this.questionFormService.oldId});

    this.watchSubmitClick();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.questionFormService.questionFormIds.push(this.questionForm.id);
      this.onChangeType('');

      if (!this.questionFormService.questionFormData[1]) {
        return;
      }
      // console.log(this.questionFormService.questionFormData[this.questionFormService.questionFormData.length - 1]);
      // this.dynamicForm.patchValue(this.questionFormService.questionFormData[this.questionFormService.questionFormData.length - 1]);
    });
    this.answerOptions = (this.dynamicForm.controls.answerOptions as FormArray);
  }

  onChangeType(e) {
    this.questionTypeName = e.value || 'multi';
    const typeCase = this.questionTypeName;

    switch (true) {
      case
      typeCase === 'short_answer' ||
      typeCase === 'paragraph' ||
      typeCase === 'file' ||
      typeCase === 'date' ||
      typeCase === 'dropdown':
        this.dynamicForm.reset();
        this.questionTypeName = e.value;
        this.clearFormArray();
        break;
      case typeCase === 'multi' || typeCase === 'checkbox':
        if ((this.dynamicForm.get('answerOptions').value).length === 0) {
          this.addOption();
        }
        break;
    }

  }

  clearFormArray() {
    (this.dynamicForm.controls.answerOptions as FormArray).clear();
  }

  addOption() {
    const answerOptions = (this.dynamicForm.controls.answerOptions as FormArray);
    answerOptions.push(new FormControl(`Option ${answerOptions.length + 1}`, Validators.required));
  }

  watchSubmitClick() {
    this.questionFormService.watchFormSubmitClick()
      .subscribe((click: boolean) => {

        if (!click) {
          return;
        }

        this.questionFormService.questionFormData.push(this.dynamicForm.value);
        this.questionFormService.formState$.next(this.questionFormService.questionFormData);

        if (this.questionFormService.addDynamicFormComp) {
          this.questionFormService.submitDynamicForm$.next(null);
        }

      });
  }

  onClick() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      // tslint:disable-next-line:prefer-for-of
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({
          data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true
        });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  cancelFile(file: FileUploadModel) {
    file.sub.unsubscribe();
    this.removeFileFromArray(file);
  }

  retryFile(file: FileUploadModel) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private uploadFile(file: FileUploadModel) {
    const fd = new FormData();
    fd.append(this.param, file.data);

    const req = new HttpRequest('POST', this.target, fd, {
      reportProgress: true
    });

    file.inProgress = true;
    file.sub = this._http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => {
      }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        this.dynamicForm.patchValue({ file: event.body.link });

        if (typeof (event) === 'object') {
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        }
      }
    );
  }

  private uploadFiles() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.value = '';

    this.files.forEach(file => {
      this.uploadFile(file);
    });
    console.log(this.files);
  }

  private removeFileFromArray(file: FileUploadModel) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }

  removeForm() {
    this.onRemoveQuestionForm.emit(this.questionForm.id);
    this.questionFormService.removeDynamicFormById(this.questionForm.id);
    this.questionFormService.questionFormIds = this.questionFormService.questionFormIds.filter(id => id !== this.questionForm.id);
  }

  copyForm() {
    this.questionFormService.save();
    this.questionFormService.addForm();
  }
}

