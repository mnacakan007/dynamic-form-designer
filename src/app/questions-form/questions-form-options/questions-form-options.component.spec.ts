import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsFormOptionsComponent } from './questions-form-options.component';

describe('QuestionsFormOptionsComponent', () => {
  let component: QuestionsFormOptionsComponent;
  let fixture: ComponentFixture<QuestionsFormOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsFormOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsFormOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
