import {Component, EventEmitter, Output} from '@angular/core';
import {QuestionFormService} from './services/question-form.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dynamic-form-designer';

  constructor(public questionFormService: QuestionFormService,
              private router: Router,
              ) {
  }

  @Output() submitForm: EventEmitter<any> = new EventEmitter<any>();

  preview() {
    this.router.navigate(['preview']);
  }

  submit() {
    this.submitForm.emit();
  }

}
