import {Subscription} from 'rxjs';

export class QuestionForms {
  questionForms: Array<any>;
  id?: number;
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}

